# non-empty-collections

Non-empty hash map and hash set implementations based on [`indexmap::IndexMap`] and
[`indexmap::IndexSet`] respectively, which are guaranteed to be non-empty by the type system of
Rust.

[![pipeline status](https://gitlab.com/mexus/non-empty-collections/badges/master/pipeline.svg)](https://gitlab.com/mexus/non-empty-collections/commits/master)
[![crates.io](https://img.shields.io/crates/v/non-empty-collections.svg)](https://crates.io/crates/non-empty-collections)
[![docs.rs](https://docs.rs/non-empty-collections/badge.svg)](https://docs.rs/non-empty-collections)

[[Release docs]](https://docs.rs/non-empty-collections/)

[[Master docs]](https://mexus.gitlab.io/non-empty-collections/non_empty_collections/)

Currently not all the methods of IndexMap or IndexSet are ported to NonEmptyIndexMap and
NonEmptyIndexSet, so if you are missing something, PRs are welcome! :)

Right now both implementations are too naïve: non-emptiness is achieved by declaring map and
set types as a pair of an element and the rest of the collection. While the idea itself is not
bad, it adds an additional overhead on basically every operation since we have to execute
everything twice: on the first element and then on the rest.

License: MIT/Apache-2.0
