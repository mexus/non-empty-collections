use serde::de::Error;
use serde::de::{Deserialize, Deserializer, SeqAccess, Visitor};
use std::fmt;
use std::hash::{BuildHasher, Hash};
use std::marker::PhantomData;
use NonEmptyIndexSet;

struct CustomSetVisitor<K, S> {
    marker: PhantomData<(K, S)>,
}

impl<K, S> CustomSetVisitor<K, S> {
    fn new() -> Self {
        CustomSetVisitor {
            marker: PhantomData,
        }
    }
}

impl<'de, K, S> Visitor<'de> for CustomSetVisitor<K, S>
where
    K: Deserialize<'de>,
    K: Eq + Hash,
    S: BuildHasher + Default,
{
    type Value = NonEmptyIndexSet<K, S>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a non-empty set")
    }

    fn visit_seq<M>(self, mut access: M) -> Result<Self::Value, M::Error>
    where
        M: SeqAccess<'de>,
    {
        let size_hint = access.size_hint().unwrap_or(0);
        let mut set = if let Some(key) = access.next_element()? {
            NonEmptyIndexSet::with_capacity_and_hasher(key, size_hint, S::default())
        } else {
            return Err(M::Error::invalid_length(0, &self));
        };
        while let Some(key) = access.next_element()? {
            set.insert(key);
        }
        Ok(set)
    }
}

impl<'de, K, S> Deserialize<'de> for NonEmptyIndexSet<K, S>
where
    K: Deserialize<'de>,
    K: Eq + Hash,
    S: BuildHasher + Default,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_seq(CustomSetVisitor::new())
    }
}
