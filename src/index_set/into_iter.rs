use indexmap::set::IntoIter as HashIter;
use std::iter::{Chain, Once};

/// A type produced by `NonEmptyIndexSet::into_iter`.
pub struct IntoIter<K> {
    pub(super) iter: Chain<Once<K>, HashIter<K>>,
}

impl<K> Iterator for IntoIter<K> {
    type Item = K;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
