use serde::ser::{Serialize, SerializeSeq, Serializer};
use std::hash::{BuildHasher, Hash};
use NonEmptyIndexSet;

impl<K, H> Serialize for NonEmptyIndexSet<K, H>
where
    K: Serialize,
    K: Eq + Hash,
    H: BuildHasher,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut set = serializer.serialize_seq(Some(self.len()))?;
        for k in self.iter() {
            set.serialize_element(k)?;
        }
        set.end()
    }
}
