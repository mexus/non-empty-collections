use std::error::Error as StdError;
use std::fmt;

/// An error that might happen during transactions.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Error {
    /// An attempt to create an empty non-empty-collection has been made :)
    EmptyCollection,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::EmptyCollection => write!(
                f,
                "An attempt to create an empty non-empty-collection has been made"
            ),
        }
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        match self {
            Error::EmptyCollection => {
                "An attempt to create an empty non-empty-collection has been made"
            }
        }
    }
}
