use serde::de::Error;
use serde::de::{Deserialize, Deserializer, MapAccess, Visitor};
use std::fmt;
use std::hash::{BuildHasher, Hash};
use std::marker::PhantomData;
use NonEmptyIndexMap;

struct CustomMapVisitor<K, V, S> {
    marker: PhantomData<(K, V, S)>,
}

impl<K, V, S> CustomMapVisitor<K, V, S> {
    fn new() -> Self {
        CustomMapVisitor {
            marker: PhantomData,
        }
    }
}

impl<'de, K, V, S> Visitor<'de> for CustomMapVisitor<K, V, S>
where
    K: Deserialize<'de>,
    V: Deserialize<'de>,
    K: Eq + Hash,
    S: BuildHasher + Default,
{
    type Value = NonEmptyIndexMap<K, V, S>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a non-empty map")
    }

    fn visit_map<M>(self, mut access: M) -> Result<Self::Value, M::Error>
    where
        M: MapAccess<'de>,
    {
        let size_hint = access.size_hint().unwrap_or(0);
        let mut map = if let Some((key, value)) = access.next_entry()? {
            NonEmptyIndexMap::with_capacity_and_hasher(key, value, size_hint, S::default())
        } else {
            return Err(M::Error::invalid_length(0, &self));
        };
        while let Some((key, value)) = access.next_entry()? {
            map.insert(key, value);
        }
        Ok(map)
    }
}

impl<'de, K, V, S> Deserialize<'de> for NonEmptyIndexMap<K, V, S>
where
    K: Deserialize<'de>,
    V: Deserialize<'de>,
    K: Eq + Hash,
    S: BuildHasher + Default,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(CustomMapVisitor::new())
    }
}
