use serde::ser::{Serialize, SerializeMap, Serializer};
use std::hash::{BuildHasher, Hash};
use NonEmptyIndexMap;

impl<K, V, H> Serialize for NonEmptyIndexMap<K, V, H>
where
    K: Serialize,
    V: Serialize,
    K: Eq + Hash,
    H: BuildHasher,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(self.len()))?;
        for (k, v) in self.iter() {
            map.serialize_entry(k, v)?;
        }
        map.end()
    }
}
