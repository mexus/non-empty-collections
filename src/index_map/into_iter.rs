use indexmap::map::IntoIter as HashIter;
use std::iter::{Chain, Once};

/// A type produced by `NonEmptyIndexMap::into_iter`.
pub struct IntoIter<K, V> {
    pub(super) iter: Chain<Once<(K, V)>, HashIter<K, V>>,
}

impl<K, V> Iterator for IntoIter<K, V> {
    type Item = (K, V);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
